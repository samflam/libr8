from pathlib import Path
import sys
import argparse
import shutil


from .fetch.cached import CachedFetcher
from .scrape.royal_road import RoyalRoadScraper
from .util import sanitize_filename

DEFAULTS = {
    "output_path": Path("libr8_out"),
    "stem_pattern": "{index:>04}_{title}",
    "narrate": False,
}


def setup_parser():
    parser = argparse.ArgumentParser("libr8")
    parser.add_argument("url")
    parser.add_argument(
        "-o",
        "--output-path",
        default=DEFAULTS["output_path"],
        type=Path,
        help=f"Directory that files will be written to [{DEFAULTS['output_path']}]",
    )
    parser.add_argument(
        "-p",
        "--stem-pattern",
        default=DEFAULTS["stem_pattern"],
        help=f"Pattern used for filenames [{DEFAULTS['stem_pattern']}]",
    )
    parser.add_argument(
        "--narrate", action="store_true", help="Also generate audio files"
    )
    return parser


def run(
    url: str,
    *,
    stem_pattern: str = DEFAULTS["stem_pattern"],
    output_path: Path = DEFAULTS["output_path"],
    narrate: bool = DEFAULTS["narrate"],
):
    output_path.mkdir(parents=True, exist_ok=True)
    fetcher = CachedFetcher(cache_path=output_path / "web_cache")
    scraper = RoyalRoadScraper(fetcher)
    documents = scraper.scrape(url)
    doc_map = {}
    for index, doc in enumerate(documents):
        stem = stem_pattern.format(index=index, title=doc.title)
        stem = sanitize_filename(stem)
        doc_map[stem] = doc

    html_path = output_path / "html"
    html_path.mkdir(exist_ok=True)
    for stem, doc in doc_map.items():
        filepath = html_path / f"{stem}.html"
        if filepath.exists():
            continue
        with open(filepath, "wb") as f:
            f.write(doc.raw)
        print(filepath)

    txt_path = output_path / "txt"
    txt_path.mkdir(exist_ok=True)
    for stem, doc in doc_map.items():
        filepath = txt_path / f"{stem}.txt"
        if filepath.exists():
            continue
        with open(filepath, "w") as f:
            f.write(doc.text)
        print(filepath)

    if narrate:
        from .narrate.gtts import GttsNarrator

        narrator = GttsNarrator()

        mp3_path = output_path / "mp3"
        mp3_path.mkdir(exist_ok=True)
        for stem, doc in doc_map.items():
            filepath = mp3_path / f"{stem}.mp3"
            if filepath.exists():
                continue
            tmp_path = filepath.with_suffix(filepath.suffix + ".part")
            audio_data = narrator.to_audio(doc.text)
            with open(tmp_path, "wb") as f:
                f.write(audio_data)
            shutil.move(tmp_path, filepath)
            print(filepath)

    return 0


parser = setup_parser()
args = parser.parse_args()
exitcode = run(**args.__dict__)
sys.exit(exitcode)
