from abc import ABC, abstractmethod


__all__ = ["IFetcher"]


class IFetcher(ABC):
    @abstractmethod
    def fetch(self, url: str) -> bytes:
        pass
