import requests
from pathlib import Path
import hashlib

from . import IFetcher


class CachedFetcher(IFetcher):
    def __init__(self, cache_path: Path | None = None):
        if cache_path:
            cache_path.mkdir(parents=True, exist_ok=True)
        self.cache_path = cache_path

    def fetch(self, url, bypass_cache=False) -> bytes:
        if not self.cache_path or bypass_cache:
            return requests.get(url).content

        m = hashlib.sha256()
        m.update(url.encode())
        url_hash = m.hexdigest()
        path = self.cache_path / f"{url_hash[:20]}"
        if path.exists():
            with open(path, "rb") as f:
                return f.read()
        content = requests.get(url).content
        with open(path, "wb") as f:
            f.write(content)
        return content
