import warnings
from typing import Iterator
import time
import io

from gtts import gTTS

from . import INarrator


class GttsNarrator(INarrator):
    BASE_RETRY_DELAY = 0.5  # sec
    BACKOFF_FACTOR = 2
    MAX_RETRIES = 17

    def _convert_line(self, line: str):
        retries = 0
        while True:
            try:
                try:
                    tts = gTTS(line)
                    fp = io.BytesIO()
                    tts.write_to_fp(fp)
                    break
                except AssertionError as err:
                    if "No text to send to TTS API" in str(err):
                        return b""
                    raise
            except Exception as err:
                if retries >= self.MAX_RETRIES:
                    raise
                delay = self.BASE_RETRY_DELAY * self.BACKOFF_FACTOR**retries
                retries += 1
                msg = (
                    f"gTTS failure {retries}/{self.MAX_RETRIES}"
                    f" - will backoff and retry in {delay}s [{err=}]"
                )
                warnings.warn(msg)
                time.sleep(delay)

        fp.seek(0)
        return fp.getvalue()

    def _merge_mp3_chunks(self, chunks: Iterator[bytes]) -> bytes:
        return b"".join(chunks)

    def to_audio(self, text: str) -> bytes:
        lines = text.strip().split("\n")
        lines = [x.strip() for x in lines]
        lines = [x for x in lines if x]
        return self._merge_mp3_chunks(self._convert_line(line) for line in lines)
