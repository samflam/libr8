from abc import abstractmethod, ABC

__all__ = ["INarrator"]


class INarrator(ABC):
    @abstractmethod
    def to_audio(text: str) -> bytes:
        pass
