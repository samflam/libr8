from urllib.parse import urlparse, urljoin

from bs4 import BeautifulSoup

from ..fetch import IFetcher
from . import Document, IScraper, CannotScrape


class RoyalRoadScraper:
    domain = "www.royalroad.com"

    def __init__(self, fetcher: IFetcher):
        self.fetcher = fetcher

    def _scrape_url_list(self, url: str) -> list[str]:
        index_html = self.fetcher.fetch(url, bypass_cache=True)
        soup = BeautifulSoup(index_html, "html.parser")
        return [
            urljoin(url, td.a["href"])
            for td in soup.find_all("td", attrs={"data-content": True})
        ]

    def _scrape_chapter(self, url: str) -> Document:
        raw = self.fetcher.fetch(url)
        soup = BeautifulSoup(raw, "html.parser")
        title = soup.title.string
        text = soup.find("div", attrs={"class": "chapter-content"}).get_text(" ")
        return Document(raw=raw, title=title, text=text)

    def scrape(self, url: str) -> list[Document]:
        if urlparse(url).netloc != self.domain:
            raise CannotScrape(f"Domain must be {self.domain}")
        url_list = self._scrape_url_list(url)
        return [self._scrape_chapter(url) for url in url_list]
