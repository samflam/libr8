from abc import ABC, abstractmethod
from dataclasses import dataclass

__all__ = ["Document", "CannotScrape", "IScraper"]


@dataclass
class Document:
    title: str
    text: str
    raw: bytes


class CannotScrape(Exception):
    pass


class IScraper(ABC):
    @abstractmethod
    def scrape(self, url: str) -> list[Document]:
        # TODO: return an iterator
        pass
