import re


def sanitize_filename(name):
    name = re.sub(r"[^a-zA-Z0-9]", " ", name)
    name = name.strip()
    name = re.sub(r"\s+", "_", name)
    return name
