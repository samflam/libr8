if [ -f "$RL_ROOT/.env" ]
then
    # Based on https://stackoverflow.com/questions/19331497
    set -a
    . "$RL_ROOT/.env"
    set +a
fi

PROJECT_NAME=libr8
VENV_DIR="$HOME/.local/share/venv"
ACTIVATE_PATH="${VENV_DIR}/${PROJECT_NAME}/bin/activate"


if [ -f "$ACTIVATE_PATH" ]
then
    source "$ACTIVATE_PATH"
fi
