PROJECT_NAME ?= libr8
PYTHON ?= python
VENV_ROOT ?= ${HOME}/.local/share/venv
VENV_DIR ?= ${VENV_ROOT}/${PROJECT_NAME}

ACTIVATE_PATH := ${VENV_DIR}/bin/activate

fmt format:
	source "${ACTIVATE_PATH}" && ${PYTHON} -m black .
.PHONY: fmt format

setup: requirements.txt | ${ACTIVATE_PATH}
	source "${ACTIVATE_PATH}" && ${PYTHON} -m pip install -r requirements.txt
.PHONY: setup

${ACTIVATE_PATH}:
	mkdir -p "${VENV_ROOT}"
	${PYTHON} -m venv "${VENV_DIR}"
