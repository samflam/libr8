# libr8

This is a web scraper with a focus on web serials. In this initial release it just supports royal road, but I'll extend it to support other hosts as the need arrises.

## Installation

TBD (there's no installer yet)

## Usage

```
usage: libr8 [-h] [-o OUTPUT_PATH] [-p STEM_PATTERN] [--narrate] url

positional arguments:
  url

options:
  -h, --help            show this help message and exit
  -o OUTPUT_PATH, --output-path OUTPUT_PATH
                        Directory that files will be written to [libr8_out]
  -p STEM_PATTERN, --stem-pattern STEM_PATTERN
                        Pattern used for filenames [{index:>04}_{title}]
  --narrate             Also generate audio files
```
